#include <iostream>
#include <iomanip>
#include "members.h"
#include "file_reader.h"
#include "constants.h"


using namespace std;
void output(member_maraphone* member_maraphone)
{
	cout << "***** �������� �������� *****\n";
	cout << member_maraphone->number << endl;
	//�������
	cout << member_maraphone->member.last_name << '\n';
	//���
	cout << member_maraphone->member.first_name << '\n';
	//��������
	cout << member_maraphone->member.middle_name << '\n';
	cout << "����� ������...: ";
	// ����� ����
	cout << setw(2) << setfill('0') << member_maraphone->start.hour << '-';
	// ����� �����
	cout << setw(2) << setfill('0') << member_maraphone->start.minutes << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << member_maraphone->start.second;
	cout << '\n';
	/********** ����� ���� �������� **********/
	// ����� ����
	cout << "����� ������...: ";
	cout << setw(2) << setfill('0') << member_maraphone->finish.hour << '-';
	// ����� �����
	cout << setw(2) << setfill('0') << member_maraphone->finish.minutes << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << member_maraphone->finish.second;
	cout << '\n';
	cout << member_maraphone->club << '\n';
}
int main()
{

	setlocale(LC_ALL, "Russian");
	member_maraphone* member[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", member, size);
		cout << "*****   *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(member[i]);
		}
		bool (*check_function)(member_maraphone*); // check_function -    ,    bool,
													//        book_subscription*
		int item;
		cin >> item;
		cout << '\n';
		
		
		
		for (int i = 0; i < size; i++)
		{
			delete member[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}

    cout << "Laboratory work #8. GIT\n";
	cout << "Variant #1. Members of maraphon\n";
	cout << "Author: Egor Cherbitch\n";
    return 0;
}

